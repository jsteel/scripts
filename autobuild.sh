#!/bin/bash
# Jonathan Steel <mail at jsteel dot org>
# http://git.jsteel.org/scripts

# A script for Arch Linux Devs/TUs to automate the task of using the build
# server:
#
#  - Copies the PKGBUILD and other files to the build server
#  - Builds the software for both architectures (unless an 'any' package)
#  - Copies the package(s) back
#
# Regular Arch users may find this useful if they have access to a faster
# computer they want to do their building on.

SERVER=pkgbuild.com
# The folder to create and build in on $SERVER:~/
DIR=packages

# Check that extra, testing or staging has been specified
if [[ $1 != 'extra' ]] && [[ $1 != 'testing' ]] && [[ $1 != 'staging' ]] || [[ -z $1 ]]; then
  echo "Error: you need to specify extra, testing or staging"
  exit 1
fi

if [[ ! -f PKGBUILD ]]; then
  echo "Error: PKGBUILD not found in current directory"
  exit 1
fi

PKG=$( cat PKGBUILD | grep '^pkgname=' | awk -F '=' '{print $2}' )
ARCH=$( cat PKGBUILD | grep 'arch=(' )
DATE=$( date +%Y%m%d-%H%M%S )

# A new directory is used each time
ssh $SERVER "mkdir -p $DIR/$PKG/$DATE"
scp ./* $SERVER:$DIR/$PKG/$DATE/

# Check if an 'any' package, otherwise build for both architectures
if [[ $ARCH == *any* ]]; then
  ssh $SERVER "cd $DIR/$PKG/$DATE/ && sudo $1-x86_64-build"
else
  ssh $SERVER "cd $DIR/$PKG/$DATE/ && sudo $1-i686-build && sudo $1-x86_64-build"
fi

# Copy the package(s) back
scp $SERVER:$DIR/$PKG/$DATE/*.tar.xz .

checkpkg

exit
